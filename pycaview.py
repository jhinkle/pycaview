#!/bin/env python2
"""Web-based image browser using XTK"""

import PyCA.Core as ca
import PyCA.Common as common

import SimpleHTTPServer
import SocketServer

import tempfile
import shutil
import os
import sys


def get_open_port():
    """Look for an open port on localhost"""
    import socket
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(("",0))
    s.listen(1)
    port = s.getsockname()[1]
    s.close()
    return port


def View(im, port=None, wait=True, browser="xdg-open"):
    """Run viewer on an image"""
    # check that user has checked out dependencies
    try:
       with open(os.path.join(os.path.dirname(__file__),'html','xtk','xtk_edge.js')): pass
    except IOError:
       print 'You have not checked out XTK yet.  From the pycaview '\
               'repository, run `git submodule update --init` and try again.'
       sys.exit(1)

    if port is None:
        port = get_open_port();

    # get temp dir
    d = tempfile.mkdtemp()

    # copy over static stuff (XTK, html)
    shutil.copytree(os.path.join(os.path.dirname(__file__),"html"),
            os.path.join(d,"pycaview"))

    # write nrrd file
    common.SaveImage(im, os.path.join(d,"pycaview","im.nrrd"))

    # launch browser
    viewerurl = "http://127.0.0.1:" + str(port)+"/pycaview/"
    print "Opening browser with address " + viewerurl
    os.system(browser + " " + viewerurl)

    # change to temp dir
    origdir = os.getcwd() 
    os.chdir(d)
    
    # start up server
    try:
        Handler = SimpleHTTPServer.SimpleHTTPRequestHandler
        httpd = SocketServer.TCPServer(("", port), Handler)
        httpd.serve_forever()
    except KeyboardInterrupt:
        print "User cancelled server.  Cleaning up and exiting..."

    os.chdir(origdir)
    shutil.rmtree(d)  # clean up temp dir

if __name__ == "__main__":
    """viewer with one argument: an image file"""
    if len(sys.argv) < 2:
        print "ERROR: Please provide the path to an image file to view"
        sys.exit(1)

    im = common.LoadImage(sys.argv[1])

    View(im)
